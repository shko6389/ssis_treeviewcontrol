﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Tree_View_control
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
          

        }

        private void ADD_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
            SqlCommand cm = new SqlCommand("SELECT *From dbo.Organization ORDER BY NodeID ASC", cn);
            try
            {
                SqlDataReader dr = cm.ExecuteReader();
                while (dr.Read())
                {
                    TreeNode node = new TreeNode(dr["NodeID"].ToString());
                    node.Nodes.Add(dr["ParentNodeID"].ToString());
                    node.Nodes.Add(dr["LevelNumber"].ToString());
                    node.Nodes.Add(dr["NodeName"].ToString());
                    node.Nodes.Add(dr["RootNodeID"].ToString());
                    node.Nodes.Add(dr["NodePath"].ToString());
                    node.Nodes.Add(dr["LastModifiedBy"].ToString());
                    node.Nodes.Add(dr["ValidFrom"].ToString());
                    node.Nodes.Add(dr["ValidTo"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.ExitThread();
            }
            }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                cn.Open();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.ExitThread();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Remove(treeView1.SelectedNode);
            treeView1.Nodes.Clear();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            string connetionString;
            SqlConnection cn;
            connetionString = @"Data Source=HDSvDevDB2016;Initial Catalog=HDSDatawarehouse;User ID=user;Password=password";
            cn = new SqlConnection(connetionString);
            cn.Open();
            MessageBox.Show("Connection Open!");
            cn.Close();
        }
    }
}
